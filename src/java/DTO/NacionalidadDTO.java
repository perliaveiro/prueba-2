
package DTO;

public class NacionalidadDTO {
    private Integer id;
    private String desc;
    private String com;

    public NacionalidadDTO() {
    }

    public NacionalidadDTO(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCom() {
        return com;
    }

    public void setCom(String com) {
        this.com = com;
    }

}
