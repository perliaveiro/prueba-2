
package DTO;

public class MotivoDTO {
    private Integer id;
    private String desc;
    
    public MotivoDTO(){
    }
    
    public MotivoDTO(Integer id){
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
