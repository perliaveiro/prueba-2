
package DTO;

public class MenuItemSistemaDTO {
    private Integer id;
    private String desc;
    private String com;

    
    private MenuSistemaDTO menuSistema;

    public MenuItemSistemaDTO() {
    }

    public MenuItemSistemaDTO(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }
    
    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public void setCom(String com) {
        this.com = com;
    }

     public String getCom() {
        return com;
    }
 

    public MenuSistemaDTO getMenuSistema() {
        return menuSistema;
    }

    public void setMenuSistema(MenuSistemaDTO menuSistema) {
        this.menuSistema = menuSistema;
    }

}
