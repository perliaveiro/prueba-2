
package DTO;

import java.sql.Date;


public class PersonaDTO {
    private Integer id;
    private Integer nroCedula;
    private String  nombres;
    private String  apellidos;
    private String  direccion;
    private Date    fechaNac;
    
    private CiudadDTO ciudad;
    private NacionalidadDTO nacionalidad;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNroCedula() {
        return nroCedula;
    }

    public void setNroCedula(Integer nroCedula) {
        this.nroCedula = nroCedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    public CiudadDTO getCiudad() {
        return ciudad;
    }

    public void setCiudad(CiudadDTO ciudad) {
        this.ciudad = ciudad;
    }

    public NacionalidadDTO getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(NacionalidadDTO nacionalidad) {
        this.nacionalidad = nacionalidad;
    }
    
    
}
