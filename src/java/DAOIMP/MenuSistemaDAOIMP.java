
package DAOIMP;

import ConexionBD.ConexionBD;
import DAO.MenuSistemaDAO;
import DTO.CiudadDTO;
import DTO.MenuSistemaDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MenuSistemaDAOIMP implements MenuSistemaDAO{
    private String sql, msj;
    private PreparedStatement ps;
    private ResultSet rs;
    private ConexionBD conec;

    public MenuSistemaDAOIMP() {
        conec = new ConexionBD();
    }

    @Override
    public Boolean agregar(MenuSistemaDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "INSERT INTO menu_sistema(mn_des, mn_com) VALUES(?, ?);";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getCom());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la insercion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean modificar(MenuSistemaDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "UPDATE menu_sistema SET mn_des = ?, mn_com =? WHERE mn_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getCom());
            ps.setInt(3, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la modificacion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean eliminar(MenuSistemaDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "DELETE FROM menu_sistema WHERE mn_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la eliminacion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<MenuSistemaDTO> recuperarRegistros() {
        try {
            List<MenuSistemaDTO> lista;
            sql = "SELECT mn_id, mn_des, mn_com FROM menu_sistema;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            lista = new ArrayList<>();
            while(rs.next()){
                MenuSistemaDTO dto = new MenuSistemaDTO();
                dto.setId(rs.getInt("mn_id"));
                dto.setDesc(rs.getString("mn_des"));
                dto.setCom(rs.getString("mn_com"));
                lista.add(dto);
            }
            
            return lista;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public MenuSistemaDTO recuperarRegistro(MenuSistemaDTO dto) {
        try {
            sql = "SELECT mn_id, mn_des, mn_com FROM menu_sistema WHERE mn_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            rs = ps.executeQuery();
            MenuSistemaDTO msistema = null;
            if(rs.next()){
                msistema = new MenuSistemaDTO();
                msistema.setId(rs.getInt("mn_id"));
                msistema.setDesc(rs.getString("mn_des"));
                msistema.setCom(rs.getString("mn_com"));
            }
            
            return msistema;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String getMsj() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
