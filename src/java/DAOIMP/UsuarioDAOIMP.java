
package DAOIMP;

import ConexionBD.ConexionBD;
import DAO.UsuarioDAO;
import DTO.PerfilDTO;
import DTO.UsuarioDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UsuarioDAOIMP implements UsuarioDAO{
    private String sql, msj;
    private PreparedStatement ps;
    private ResultSet rs;
    private ConexionBD conec;
    
    public UsuarioDAOIMP(){
        conec = new ConexionBD();
    }

    @Override
    public Boolean agregar(UsuarioDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "INSERT INTO usuario(usu_des, usu_log, usu_pass, usu_estado, per_id) VALUES(?, ?, ?, ?, ?);";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getLog());
            ps.setString(3, dto.getPass());
            ps.setBoolean(4, dto.getEst());
            ps.setInt(5, dto.getPerfil().getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la insercion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean modificar(UsuarioDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "UPDATE usuario SET usu_des = ?, usu_log =?, usu_pass = ?, usu_estado = ?, "
                    + "per_id = ? WHERE usu_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getLog());
            ps.setString(3, dto.getPass());
            ps.setBoolean(4, dto.getEst());
            ps.setInt(5, dto.getPerfil().getId());
            ps.setInt(6, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la modificacion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean eliminar(UsuarioDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "DELETE FROM usuario WHERE usu_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la eliminacion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<UsuarioDTO> recuperarRegistros() {
        try {
            List<UsuarioDTO> lista;
            sql = "SELECT u.usu_id, u.usu_des, u.usu_log, u.usu_pass, u.usu_estado, u.per_id, "
                    + "p.per_des FROM usuario u, perfiles p WHERE u.per_id = p.per_id;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            lista = new ArrayList<>();
            while(rs.next()){
                UsuarioDTO dto = new UsuarioDTO();
                dto.setId(rs.getInt("usu_id"));
                dto.setDesc(rs.getString("usu_des"));
                dto.setLog(rs.getString("usu_log"));
                dto.setPass(rs.getString("usu_pass"));
                dto.setEst(rs.getBoolean("usu_estado"));
                PerfilDTO perfil = new PerfilDTO();
                    perfil.setId(rs.getInt("per_id"));
                    perfil.setDesc(rs.getString("per_des"));
                dto.setPerfil(perfil);
                lista.add(dto);
            }
            
            return lista;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public UsuarioDTO recuperarRegistro(UsuarioDTO dto) {
        try {
            sql = "SELECT u.usu_id, u.usu_des, u.usu_log, u.usu_pass, u.usu_estado, u.per_id, "
                    + "p.per_des FROM usuario u, perfiles p WHERE u.per_id = p.per_id "
                    + "AND u.usu_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            rs = ps.executeQuery();
            UsuarioDTO usuario = null;
            if(rs.next()){
                usuario = new UsuarioDTO();
                usuario.setId(rs.getInt("usu_id"));
                usuario.setDesc(rs.getString("usu_des"));
                usuario.setLog(rs.getString("usu_log"));
                usuario.setPass(rs.getString("usu_pass"));
                usuario.setEst(rs.getBoolean("usu_estado"));
                PerfilDTO perfil = new PerfilDTO();
                    perfil.setId(rs.getInt("per_id"));
                    perfil.setDesc(rs.getString("per_des"));
                usuario.setPerfil(perfil);
            }
            
            return usuario;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String getMsj() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
