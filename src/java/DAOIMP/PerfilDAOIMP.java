
package DAOIMP;

import ConexionBD.ConexionBD;
import DAO.PerfilDAO;
import DTO.CiudadDTO;
import DTO.PerfilDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PerfilDAOIMP implements PerfilDAO{
    private String sql, msj;
    private PreparedStatement ps;
    private ResultSet rs;
    private ConexionBD conec;
    
    public PerfilDAOIMP(){
        conec = new ConexionBD();
    }

    @Override
    public Boolean agregar(PerfilDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "INSERT INTO perfiles(per_des, per_com) VALUES(?, ?);";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getCom());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la insercion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean modificar(PerfilDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "UPDATE perfiles SET per_des = ?, per_com =? WHERE per_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getCom());
            ps.setInt(3, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la modificacion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean eliminar(PerfilDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "DELETE FROM perfiles WHERE per_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la eliminacion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<PerfilDTO> recuperarRegistros() {
        try {
            List<PerfilDTO> lista;
            sql = "SELECT per_id, per_des, per_com FROM perfiles;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            lista = new ArrayList<>();
            while(rs.next()){
                PerfilDTO dto = new PerfilDTO();
                dto.setId(rs.getInt("per_id"));
                dto.setDesc(rs.getString("per_des"));
                dto.setCom(rs.getString("per_com"));
                lista.add(dto);
            }
            
            return lista;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public PerfilDTO recuperarRegistro(PerfilDTO dto) {
        try {
            sql = "SELECT per_id, per_des, per_com FROM perfiles WHERE per_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            rs = ps.executeQuery();
            PerfilDTO perfil = null;
            if(rs.next()){
                perfil = new PerfilDTO();
                perfil.setId(rs.getInt("per_id"));
                perfil.setDesc(rs.getString("per_des"));
                perfil.setCom(rs.getString("per_com"));
            }
            
            return perfil;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String getMsj() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
