
package DAOIMP;

import ConexionBD.ConexionBD;
import ConexionBD.ConexionBD.TR;
import DAO.DireccionDAO;
import DTO.DireccionDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DireccionDAOIMP implements DireccionDAO{
    private String sql, msj;
    private PreparedStatement ps;
    private ResultSet rs;
    private ConexionBD conec;
    
    public DireccionDAOIMP(){
        conec = new ConexionBD();
    }

    @Override
    public Boolean agregar(DireccionDTO dto) {
        try {
            conec.Transaccion(TR.INICIAR);
            sql = "INSERT INTO direccion(dire_desc, dire_com) VALUES(?, ?);";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getCom());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la insercion: " + ex.getMessage();
            conec.Transaccion(TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean modificar(DireccionDTO dto) {
        try {
            conec.Transaccion(TR.INICIAR);
            sql = "UPDATE direccion SET dire_desc = ?, dire_com =? WHERE dire_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getCom());
            ps.setInt(3, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la modificacion: " + ex.getMessage();
            conec.Transaccion(TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean eliminar(DireccionDTO dto) {
        try {
            conec.Transaccion(TR.INICIAR);
            sql = "DELETE FROM direccion WHERE dire_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la eliminacion: " + ex.getMessage();
            conec.Transaccion(TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<DireccionDTO> recuperarRegistros() {
        try {
            List<DireccionDTO> lista;
            sql = "SELECT dire_id, dire_desc, dire_com FROM direccion;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            lista = new ArrayList<>();
            while(rs.next()){
                DireccionDTO dto = new DireccionDTO();
                dto.setId(rs.getInt("dire_id"));
                dto.setDesc(rs.getString("dire_desc"));
                dto.setCom(rs.getString("dire_com"));
                lista.add(dto);
            }
            
            return lista;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public DireccionDTO recuperarRegistro(DireccionDTO dto) {
        try {
            sql = "SELECT dire_id, dire_desc, dire_com FROM direccion WHERE dire_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            rs = ps.executeQuery();
            DireccionDTO direccion = null;
            if(rs.next()){
                direccion = new DireccionDTO();
                direccion.setId(rs.getInt("dire_id"));
                direccion.setDesc(rs.getString("dire_desc"));
                direccion.setCom(rs.getString("dire_com"));
            }
            
            return direccion;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String getMsj() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
