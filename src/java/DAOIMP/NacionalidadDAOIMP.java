
package DAOIMP;

import ConexionBD.ConexionBD;
import DAO.NacionalidadDAO;
import DTO.CiudadDTO;
import DTO.NacionalidadDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NacionalidadDAOIMP implements NacionalidadDAO{
    private String sql, msj;
    private PreparedStatement ps;
    private ResultSet rs;
    private ConexionBD conec;
    
    public NacionalidadDAOIMP(){
        conec = new ConexionBD();
    }

    @Override
    public Boolean agregar(NacionalidadDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "INSERT INTO nacionalidad(nacio_des, nacio_com) VALUES(?, ?);";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getCom());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la insercion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean modificar(NacionalidadDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "UPDATE nacionalidad SET nacio_des = ?, nacio_com =? WHERE id_nacio = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getCom());
            ps.setInt(3, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la modificacion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean eliminar(NacionalidadDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "DELETE FROM nacionalidad WHERE id_nacio = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la eliminacion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<NacionalidadDTO> recuperarRegistros() {
        try {
            List<NacionalidadDTO> lista;
            sql = "SELECT id_nacio, nacio_des, nacio_com FROM nacionalidad;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            lista = new ArrayList<>();
            while(rs.next()){
                NacionalidadDTO dto = new NacionalidadDTO();
                dto.setId(rs.getInt("id_nacio"));
                dto.setDesc(rs.getString("nacio_des"));
                dto.setCom(rs.getString("nacio_com"));
                lista.add(dto);
            }
            
            return lista;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public NacionalidadDTO recuperarRegistro(NacionalidadDTO dto) {
        try {
            sql = "SELECT id_nacio, nacio_des, nacio_com FROM nacionalidad WHERE id_nacio = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            rs = ps.executeQuery();
            NacionalidadDTO nacionalidad = null;
            if(rs.next()){
                nacionalidad = new NacionalidadDTO();
                nacionalidad.setId(rs.getInt("id_nacio"));
                nacionalidad.setDesc(rs.getString("nacio_des"));
                nacionalidad.setCom(rs.getString("nacio_com"));
            }
            
            return nacionalidad;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String getMsj() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
