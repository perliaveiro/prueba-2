
package DAOIMP;

import ConexionBD.ConexionBD;
import ConexionBD.ConexionBD.TR;
import DAO.CiudadDAO;
import DTO.CiudadDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CiudadDAOIMP implements CiudadDAO{
    private String sql, msj;
    private PreparedStatement ps;
    private ResultSet rs;
    private ConexionBD conec;
    
    public CiudadDAOIMP(){
        conec = new ConexionBD();
    }

    @Override
    public Boolean agregar(CiudadDTO dto) {
        try {
            conec.Transaccion(TR.INICIAR);
            sql = "INSERT INTO ciudad(ciu_desc, ciu_com) VALUES(?, ?);";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getCom());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la insercion: " + ex.getMessage();
            conec.Transaccion(TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean modificar(CiudadDTO dto) {
        try {
            conec.Transaccion(TR.INICIAR);
            sql = "UPDATE ciudad SET ciu_desc = ?, ciu_com =? WHERE ciu_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getCom());
            ps.setInt(3, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la modificacion: " + ex.getMessage();
            conec.Transaccion(TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean eliminar(CiudadDTO dto) {
        try {
            conec.Transaccion(TR.INICIAR);
            sql = "DELETE FROM ciudad WHERE ciu_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la eliminacion: " + ex.getMessage();
            conec.Transaccion(TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<CiudadDTO> recuperarRegistros() {
        try {
            List<CiudadDTO> lista;
            sql = "SELECT ciu_id, ciu_desc, ciu_com FROM ciudad;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            lista = new ArrayList<>();
            while(rs.next()){
                CiudadDTO dto = new CiudadDTO();
                dto.setId(rs.getInt("ciu_id"));
                dto.setDesc(rs.getString("ciu_desc"));
                dto.setCom(rs.getString("ciu_com"));
                lista.add(dto);
            }
            
            return lista;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public CiudadDTO recuperarRegistro(CiudadDTO dto) {
        try {
            sql = "SELECT ciu_id, ciu_desc, ciu_com FROM ciudad WHERE ciu_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            rs = ps.executeQuery();
            CiudadDTO ciudad = null;
            if(rs.next()){
                ciudad = new CiudadDTO();
                ciudad.setId(rs.getInt("ciu_id"));
                ciudad.setDesc(rs.getString("ciu_desc"));
                ciudad.setCom(rs.getString("ciu_com"));
            }
            
            return ciudad;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String getMsj() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
