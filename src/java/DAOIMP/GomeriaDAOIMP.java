
package DAOIMP;

import ConexionBD.ConexionBD;
import DAO.GomeriaDAO;
import DTO.CiudadDTO;
import DTO.DireccionDTO;
import DTO.GomeriaDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GomeriaDAOIMP implements GomeriaDAO{
    private String sql, msj;
    private PreparedStatement ps;
    private ResultSet rs;
    private ConexionBD conec;

    public GomeriaDAOIMP() {
        conec = new ConexionBD();
    }

    @Override
    public Boolean agregar(GomeriaDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "INSERT INTO gomeria(gom_desc, gom_horaape, gom_horacie, gom_telf, "
                    + "gom_titular, dire_id, ciu_id) VALUES(?, ?, ?, ?, ?, ?, ?);";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getApe());
            ps.setString(3, dto.getCie());
            ps.setString(4, dto.getTel());
            ps.setString(5, dto.getTitu());
            ps.setInt(6, dto.getDireccion().getId());
            ps.setInt(7, dto.getCiudad().getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la insercion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean modificar(GomeriaDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "UPDATE gomeria SET gom_desc = ?, gom_horaape =?, gom_horacie = ?, gom_telf = ?, "
                    + "gom_titular = ?, dire_id = ?, ciu_id = ? WHERE gom_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getApe());
            ps.setString(3, dto.getCie());
            ps.setString(4, dto.getTel());
            ps.setString(5, dto.getTitu());
            ps.setInt(6, dto.getDireccion().getId());
            ps.setInt(7, dto.getCiudad().getId());
            ps.setInt(8, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la modificacion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean eliminar(GomeriaDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "DELETE FROM gomeria WHERE gom_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la eliminacion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<GomeriaDTO> recuperarRegistros() {
        try {
            List<GomeriaDTO> lista;
            sql = "SELECT g.gom_id, g.gom_desc, g.gom_horaape, g.gom_horacie, g.gom_telf, g.gom_titular, g.dire_id,\n" +
" d.dire_desc, g.ciu_id, c.ciu_desc FROM gomeria g, ciudad c, direccion d\n" +
" WHERE g.ciu_id = c.ciu_id AND g.dire_id = d.dire_id;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            lista = new ArrayList<>();
            while(rs.next()){
                GomeriaDTO dto = new GomeriaDTO();
                dto.setId(rs.getInt("gom_id"));
                dto.setDesc(rs.getString("gom_desc"));
                dto.setApe(rs.getString("gom_horaape"));
                dto.setCie(rs.getString("gom_horacie"));
                dto.setTel(rs.getString("gom_telf"));
                dto.setTitu(rs.getString("gom_titular"));
                DireccionDTO direccion = new DireccionDTO();
                    direccion.setId(rs.getInt("dire_id"));
                    direccion.setDesc(rs.getString("dire_desc"));
                dto.setDireccion(direccion);
                CiudadDTO ciudad = new CiudadDTO();
                    ciudad.setId(rs.getInt("ciu_id"));
                    ciudad.setDesc(rs.getString("ciu_desc"));
                dto.setCiudad(ciudad);
                lista.add(dto);
            }
            
            return lista;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public GomeriaDTO recuperarRegistro(GomeriaDTO dto) {
        try {
            sql = "SELECT g.gom_id, g.gom_desc, g.gom_horaape, g.gom_horacie, g.gom_telf, g.gom_titular, g.dire_id,\n" +
" d.dire_desc, g.ciu_id, c.ciu_desc FROM gomeria g, ciudad c, direccion d\n" +
" WHERE g.ciu_id = c.ciu_id AND g.dire_id = d.dire_id  AND g.gom_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            rs = ps.executeQuery();
            GomeriaDTO gomeria = null;
            if(rs.next()){
                gomeria = new GomeriaDTO();
                gomeria.setId(rs.getInt("gom_id"));
                gomeria.setDesc(rs.getString("gom_desc"));
                gomeria.setApe(rs.getString("gom_horaape"));
                gomeria.setCie(rs.getString("gom_horacie"));
                gomeria.setTel(rs.getString("gom_telf"));
                gomeria.setTitu(rs.getString("gom_titular"));
                DireccionDTO direccion = new DireccionDTO();
                    direccion.setId(rs.getInt("dire_id"));
                    direccion.setDesc(rs.getString("dire_desc"));
                gomeria.setDireccion(direccion);
                CiudadDTO ciudad = new CiudadDTO();
                    ciudad.setId(rs.getInt("ciu_id"));
                    ciudad.setDesc(rs.getString("ciu_desc"));
                gomeria.setCiudad(ciudad);
            }
            
            return gomeria;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String getMsj() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
