
package DAOIMP;

import ConexionBD.ConexionBD;
import DTO.MenuSistemaDTO;
import DAO.MenuItemSistemaDAO;
import DTO.MenuItemSistemaDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MenuItemSistemaDAOIMP implements MenuItemSistemaDAO{
    private String sql, msj;
    private PreparedStatement ps;
    private ResultSet rs;
    private ConexionBD conec;
    
    public MenuItemSistemaDAOIMP(){
        conec = new ConexionBD();
    }

    @Override
    public Boolean agregar(MenuItemSistemaDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "INSERT INTO menu_items_sistema(mni_des, mni_com, mn_id) VALUES(?, ?, ?);";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getCom());
            ps.setInt(3, dto.getMenuSistema().getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la insercion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean modificar(MenuItemSistemaDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "UPDATE menu_items_sistema SET mni_des = ?, mni_com =?, mn_id = ? WHERE mni_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setString(1, dto.getDesc());
            ps.setString(2, dto.getCom());
            ps.setInt(3, dto.getMenuSistema().getId());
            ps.setInt(4, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la modificacion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean eliminar(MenuItemSistemaDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "DELETE FROM menu_items_sistema WHERE mni_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            if(ps.executeUpdate() > 0){
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la eliminacion: " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<MenuItemSistemaDTO> recuperarRegistros() {
        try {
            List<MenuItemSistemaDTO> lista;
            sql = "SELECT m.mni_id, m.mni_des, m.mni_com, m.mn_id,\n" +
"n.mn_des FROM menu_items_sistema m, menu_sistema n WHERE m.mn_id = n.mn_id;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            lista = new ArrayList<>();
            while(rs.next()){
                MenuItemSistemaDTO dto = new MenuItemSistemaDTO();
                dto.setId(rs.getInt("mni_id"));
                dto.setDesc(rs.getString("mni_des"));
                dto.setCom(rs.getString("mni_com"));
                MenuSistemaDTO menuSistema = new MenuSistemaDTO();
                    menuSistema.setId(rs.getInt("mn_id"));
                    menuSistema.setDesc(rs.getString("mn_des"));
                dto.setMenuSistema(menuSistema);
                lista.add(dto);
            }
            
            return lista;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public MenuItemSistemaDTO recuperarRegistro(MenuItemSistemaDTO dto) {
        try {
            sql = "SELECT m.mni_id, m.mni_des, m.mni_com, m.mn_id,n.mn_des FROM menu_items_sistema m, menu_sistema n WHERE m.mn_id = n.mn_id AND m.mni_id = ?;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            rs = ps.executeQuery();
            MenuItemSistemaDTO menuItemSistema = null;
            if(rs.next()){
                menuItemSistema = new MenuItemSistemaDTO();
                menuItemSistema.setId(rs.getInt("mni_id"));
                menuItemSistema.setDesc(rs.getString("mni_des"));
                menuItemSistema.setCom(rs.getString("mni_com"));
                MenuSistemaDTO menuSistema = new MenuSistemaDTO();
                    menuSistema.setId(rs.getInt("mn_id"));
                    menuSistema.setDesc(rs.getString("mn_des"));
                menuItemSistema.setMenuSistema(menuSistema);
            }
            
            return menuItemSistema;
        } catch (SQLException ex) {
            msj = "Error en el select: " + ex.getMessage();
            return null;
        }
        
        finally{
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String getMsj() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
