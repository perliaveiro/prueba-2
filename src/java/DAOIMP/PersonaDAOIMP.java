package DAOIMP;

import ConexionBD.ConexionBD;
import DAO.PersonaDAO;
import DTO.CiudadDTO;
import DTO.NacionalidadDTO;
import DTO.PerfilDTO;
import DTO.PersonaDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PersonaDAOIMP implements PersonaDAO {

    private String sql, msj;
    private PreparedStatement ps;
    private ResultSet rs;
    private ConexionBD conec;

    public PersonaDAOIMP() {
        conec = new ConexionBD();
    }

    @Override
    public Boolean agregar(PersonaDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = " INSERT INTO personas(per_ci, per_nom, per_ape, per_dire, per_fna, ciu_id, id_nacio) VALUES ( ?, ?, ?, ?, ?, ?, ?);";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getNroCedula());
            ps.setString(2, dto.getNombres());
            ps.setString(3, dto.getApellidos());
            ps.setString(4, dto.getDireccion());
            ps.setDate(5, dto.getFechaNac());
            ps.setInt(6, dto.getCiudad().getId());
            ps.setInt(7, dto.getNacionalidad().getId());
            if (ps.executeUpdate() > 0) {
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la inserción " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        } finally {
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(PerfilDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean modificar(PersonaDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "UPDATE personas SET  per_ci=?, per_nom=?, per_ape=?, per_dire=?, per_fna=?, ciu_id=?, id_nacio=? WHERE id_per=?; ";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getNroCedula());
            ps.setString(2, dto.getNombres());
            ps.setString(3, dto.getApellidos());
            ps.setString(4, dto.getDireccion());
            ps.setDate(5, dto.getFechaNac());
            ps.setInt(6, dto.getCiudad().getId());
            ps.setInt(7, dto.getNacionalidad().getId());
            ps.setInt(8, dto.getId());
            if (ps.executeUpdate() > 0) {
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la modificación " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        } finally {
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(PerfilDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Boolean eliminar(PersonaDTO dto) {
        try {
            conec.Transaccion(ConexionBD.TR.INICIAR);
            sql = "DELETE FROM  personas  WHERE id_per=?; ";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, dto.getId());
            if (ps.executeUpdate() > 0) {
                conec.Transaccion(ConexionBD.TR.CONFIRMAR);
                return true;
            } else {
                conec.Transaccion(ConexionBD.TR.CANCELAR);
                return false;
            }
        } catch (SQLException ex) {
            msj = "Error en la eliminación " + ex.getMessage();
            conec.Transaccion(ConexionBD.TR.CANCELAR);
            return false;
        } finally {
            try {
                conec.cerrarConexion();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(PerfilDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<PersonaDTO> recuperarRegistros() {

        try {
            List<PersonaDTO> lista;
            sql = " SELECT p.id_per, p.per_ci, p.per_nom, p.per_ape, p.per_dire, p.per_fna, p.id_nacio, \n" +
"n.nacio_des, p.ciu_id, c.ciu_desc FROM personas p, ciudad c, nacionalidad n \n" +
"WHERE p.ciu_id = c.ciu_id AND p.id_nacio = n.id_nacio";
            ps = conec.obtenerConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            lista = new ArrayList<>();
            while (rs.next()) {
                PersonaDTO dto = new PersonaDTO();
                dto.setId(rs.getInt("id_per"));
                dto.setNroCedula(rs.getInt("per_ci"));
                dto.setNombres(rs.getString("per_nom"));
                dto.setApellidos(rs.getString("per_ape"));
                dto.setDireccion(rs.getString("per_dire"));
                dto.setFechaNac(rs.getDate("per_fna"));
                CiudadDTO ciudad = new CiudadDTO();
                    ciudad.setId(rs.getInt("ciu_id"));
                    ciudad.setDesc(rs.getString("ciu_desc"));
                dto.setCiudad(ciudad);
                NacionalidadDTO nacionalidad = new NacionalidadDTO();
                    nacionalidad.setId(rs.getInt("id_nacio"));
                    nacionalidad.setDesc(rs.getString("nacio_des"));
                dto.setNacionalidad(nacionalidad);
                lista.add(dto);
            }
            return lista;
        } catch (SQLException ex) {
            msj = "Error al recuperar registros " + ex.getMessage();
            return null;
        } finally {
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(PerfilDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    @Override
    public PersonaDTO recuperarRegistro(PersonaDTO x) {
        try {
            PersonaDTO dto = null;
            sql = " SELECT p.id_per, p.per_ci, p.per_nom, p.per_ape, p.per_dire, p.per_fna, p.id_nacio,\n" +
"n.nacio_des, p.ciu_id, c.ciu_desc FROM personas p, ciudad c, nacionalidad n\n" +
"WHERE p.ciu_id = c.ciu_id AND p.id_nacio = n.id_nacio AND p.id_per = ? ;";
            ps = conec.obtenerConexion().prepareStatement(sql);
            ps.setInt(1, x.getId());
            rs = ps.executeQuery();
            if (rs.next()) {
                dto = new PersonaDTO();
                dto.setId(rs.getInt("id_per"));
                dto.setNroCedula(rs.getInt("per_ci"));
                dto.setNombres(rs.getString("per_nom"));
                dto.setApellidos(rs.getString("per_ape"));
                dto.setDireccion(rs.getString("per_dire"));
                dto.setFechaNac(rs.getDate("per_fna"));
                CiudadDTO ciudad = new CiudadDTO();
                    ciudad.setId(rs.getInt("ciu_id"));
                    ciudad.setDesc(rs.getString("ciu_desc"));
                dto.setCiudad(ciudad);
                NacionalidadDTO nacionalidad = new NacionalidadDTO();
                    nacionalidad.setId(rs.getInt("id_nacio"));
                    nacionalidad.setDesc(rs.getString("nacio_des"));
                dto.setNacionalidad(nacionalidad);
            }
            return dto;
        } catch (SQLException ex) {
            msj = "Error al recuperar registros " + ex.getMessage();
            return null;
        } finally {
            try {
                conec.cerrarConexion();
                ps.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(PerfilDAOIMP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String getMsj() {
        return msj;
    }

}
