package Test;
import DAO.BaseSQL;
import DAO.CiudadDAO;
import DAO.MenuSistemaDAO;
import DAO.MotivoDAO;
import DAO.PerfilDAO;
import DAO.NacionalidadDAO;
import DAO.DireccionDAO;
import DAO.GomeriaDAO;
import DAO.UsuarioDAO;
import DAO.MenuItemSistemaDAO;
import DAO.PersonaDAO;

import DAOIMP.CiudadDAOIMP;
import DAOIMP.MenuSistemaDAOIMP;
import DAOIMP.MotivoDAOIMP;
import DAOIMP.PerfilDAOIMP;
import DAOIMP.NacionalidadDAOIMP;
import DAOIMP.DireccionDAOIMP;
import DAOIMP.GomeriaDAOIMP;
import DAOIMP.UsuarioDAOIMP;
import DAOIMP.MenuItemSistemaDAOIMP;
import DAOIMP.PersonaDAOIMP;

import DTO.CiudadDTO;
import DTO.MenuSistemaDTO;
import DTO.MotivoDTO;
import DTO.NacionalidadDTO;
import DTO.PerfilDTO;
import DTO.DireccionDTO;
import DTO.GomeriaDTO;
import DTO.UsuarioDTO;
import DTO.MenuItemSistemaDTO;
import DTO.PersonaDTO;

import java.sql.Date;


public class Test {
    
    

    
    CiudadDAO ciudadDAO;
    CiudadDTO ciudadDTO;
    
    
    MenuSistemaDAO menuSistemaDAO; 
    MenuSistemaDTO menuSistemaDTO;
    
    
    MotivoDAO motivoDAO;
    MotivoDTO motivoDTO;
    
    NacionalidadDTO nacionalidadDTO;
    NacionalidadDAO nacionalidadDAO;
    
    
    PerfilDAO perfilDAO;
    PerfilDTO perfilDTO, perfil; 
    
    
    DireccionDAO direccionDAO;
    DireccionDTO direccionDTO;
    
    
    GomeriaDAO gomeriaDAO;
    GomeriaDTO gomeriaDTO;
    
    
    UsuarioDTO usuarioDTO;
    UsuarioDAO usuarioDAO;  
    
    
    MenuItemSistemaDAO menuitemSistemaDAO; 
    MenuItemSistemaDTO menuitemSistemaDTO;
    
    PersonaDTO personaDTO;
    PersonaDAO personaDAO;
    

    
    public Test() {
        
        
        
        
        
//CIUDAD--------------------------------------------------------------------------------
//        
//      ciudadDTO = new CiudadDTO();   //si
//       ciudadDTO.setId(5);            //si
////        ciudadDTO.setDesc("Capiata");
////        ciudadDTO.setCom("comentario");
////        
//        ciudadDAO = new CiudadDAOIMP();     //si
////        
////        ciudadDAO.eliminar(ciudadDTO);                    //modificar agregar eliminar
        
        
                
//        for (CiudadDTO dto : ciudadDAO.recuperarRegistros()) {            //sin id
//            System.out.println("ID: " + dto.getId());
//            System.out.println("CIUDAD: " + dto.getDesc());
//            System.out.println("COMENTARIO: " + dto.getCom());
//            System.out.println("------------------------------------------------");
//        }  

////
//        CiudadDTO dto =ciudadDAO.recuperarRegistro(ciudadDTO);    //con id
//        System.out.println("ID: " + dto.getId());
//        System.out.println("CIUDAD: " + dto.getDesc());
//        System.out.println("COMENTARIO: " + dto.getCom());
//        System.out.println("------------------------------------------------");





//MENU SISTEMA------------------------------------------------------------------

//        menuSistemaDTO = new MenuSistemaDTO();     //si      
//        menuSistemaDTO.setId(1);                    //si
////        menuSistemaDTO.setDesc("ASDAS");
////        menuSistemaDTO.setCom("OOOOOAAA");
////        
//        menuSistemaDAO = new MenuSistemaDAOIMP();      //si     
//        
//        menuSistemaDAO.eliminar(menuSistemaDTO);                 //modificar agregar eliminar  
        
        
       
//        for (MenuSistemaDTO dto : menuSistemaDAO.recuperarRegistros()) {            //sin id
//            System.out.println("ID: " + dto.getId());
//            System.out.println("MENU SISTEMA: " + dto.getDesc());
//            System.out.println("COMENTARIO: " + dto.getCom());
//            System.out.println("------------------------------------------------");
//        }  
// 
  
        
//       MenuSistemaDTO dto = menuSistemaDAO.recuperarRegistro(menuSistemaDTO);    //con id
//        System.out.println("ID: " + dto.getId());
//        System.out.println("MENU SISTEMA: " + dto.getDesc());
//        System.out.println("COMENTARIO: " + dto.getCom());
//        System.out.println("------------------------------------------------");
//    





//MOTIVO-----------------------------------------------------------------------
    
    
//        motivoDTO = new MotivoDTO();     //si      
//        motivoDTO.setId(2);                    //si
//        motivoDTO.setDesc("Inflar Rueda");
 
//        
//        motivoDAO = new MotivoDAOIMP();      //si     
        
//        motivoDAO.eliminar(motivoDTO);              //modificar agregar eliminar

        
        
//            for (MotivoDTO dto : motivoDAO.recuperarRegistros()) {            //sin id
//            System.out.println("ID: " + dto.getId());
//            System.out.println("MOTIVO: " + dto.getDesc());
//            System.out.println("------------------------------------------------");
//        }  
 
  
//        
//       MotivoDTO dto = motivoDAO.recuperarRegistro(motivoDTO);    //con id
//        System.out.println("ID: " + dto.getId());
//        System.out.println("MOTIVO: " + dto.getDesc());
//        System.out.println("------------------------------------------------");
//        
        
        
        
//NACIONALIDAD-------------------------------------------------------------------------    
        


//        nacionalidadDTO = new NacionalidadDTO();     //si      
//        nacionalidadDTO.setId(1);                    //si
////        nacionalidadDTO.setDesc("Argentina");
////        nacionalidadDTO.setCom("Comentario");
//        
//        nacionalidadDAO = new NacionalidadDAOIMP();      //si     
        
//        nacionalidadDAO.modificar(nacionalidadDTO);                   //modificar agregar eliminar          
        
        
       
//        for (NacionalidadDTO dto : nacionalidadDAO.recuperarRegistros()) {            //sin id
//            System.out.println("ID: " + dto.getId());
//            System.out.println("NACIONALIDAD: " + dto.getDesc());
//            System.out.println("COMENTARIO: " + dto.getCom());
//            System.out.println("------------------------------------------------");
//        }  
 
  
//        
//       NacionalidadDTO dto = nacionalidadDAO.recuperarRegistro(nacionalidadDTO);    //con id
//        System.out.println("ID: " + dto.getId());
//        System.out.println("NACIONALIDAD: " + dto.getDesc());
//        System.out.println("COMENTARIO: " + dto.getCom());
//        System.out.println("------------------------------------------------"); 
        
        
        
        
   
//PERFIL-------------------------------------------------------------------------    
        

//
//        perfilDTO = new PerfilDTO();     //si      
//        perfilDTO.setId(2);                    //si
//        perfilDTO.setDesc("perla");
//        perfilDTO.setCom("Comentario");
//        
//        perfilDAO = new PerfilDAOIMP();      //si     
        
//        perfilDAO.eliminar(perfilDTO);                    //modificar agregar eliminar
        
        
       
//        for (PerfilDTO dto : perfilDAO.recuperarRegistros()) {            //sin id
//            System.out.println("ID: " + dto.getId());
//            System.out.println("PERFIL: " + dto.getDesc());
//            System.out.println("COMENTARIO: " + dto.getCom());
//            System.out.println("------------------------------------------------");
//        }  
 
  
        
//       PerfilDTO dto = perfilDAO.recuperarRegistro(perfilDTO);    //con id
//        System.out.println("ID: " + dto.getId());
//        System.out.println("PERFIL: " + dto.getDesc());
//        System.out.println("COMENTARIO: " + dto.getCom());
//        System.out.println("------------------------------------------------"); 






//DIRECCION-------------------------------------------------------------------------    
        

    
//        direccionDTO = new DireccionDTO();     //si      
//        direccionDTO.setId(3);                    //si
//        direccionDTO.setDesc("Calle San Cayetano");
//        direccionDTO.setCom("dasdasd");
        
//        direccionDAO = new DireccionDAOIMP();      //si     
        
//        direccionDAO.eliminar(direccionDTO);                   //modificar agregar eliminar
        
        
       
//        for (DireccionDTO dto : direccionDAO.recuperarRegistros()) {            //sin id
//            System.out.println("ID: " + dto.getId());
//            System.out.println("DIRECCION: " + dto.getDesc());
//            System.out.println("COMENTARIO: " + dto.getCom());
//            System.out.println("------------------------------------------------");
//        }  
// 
  
//        
//       DireccionDTO dto = direccionDAO.recuperarRegistro(direccionDTO);    //con id
//        System.out.println("ID: " + dto.getId());
//        System.out.println("DIRECCION: " + dto.getDesc());
//        System.out.println("COMENTARIO: " + dto.getCom());
//        System.out.println("------------------------------------------------"); 



//GOMERIA--------------------------------------------------------------------------

//
//        gomeriaDTO = new GomeriaDTO();     //si
//        gomeriaDTO.setId(2);                      //si
//        gomeriaDTO.setDesc("GOMERIA LAS RUEDAS :D");
//        gomeriaDTO.setApe("07:00");
//        gomeriaDTO.setCie("00:00");
//        gomeriaDTO.setTel("0982572507");
//        gomeriaDTO.setTitu("CRISTIAN GONZALEZ GOMEZ");
//
//        CiudadDTO ciudad = new CiudadDTO();
//        ciudad.setId(2);
//        gomeriaDTO.setCiudad(ciudad);
//        
//        DireccionDTO direccion = new DireccionDTO();
//        direccion.setId(1);
//        gomeriaDTO.setDireccion(direccion);
//        
//        gomeriaDTO.setCiudad(new CiudadDTO(2));                     //ID
//        gomeriaDTO.setDireccion(new DireccionDTO(2));               //ID
//
//        gomeriaDAO = new GomeriaDAOIMP();                         //si
         
//        gomeriaDAO.eliminar(gomeriaDTO);                               //modificar agregar eliminar
////        
//        
////        
//        for (GomeriaDTO dto : gomeriaDAO.recuperarRegistros()) {              //sin id
//            System.out.println("Id " + dto.getId() );
//            System.out.println("DESCRIPCION:" + dto.getDesc());
//            System.out.println("HORA DE APERTURA:" + dto.getApe());
//            System.out.println("HORA DE CIERRE:" + dto.getCie());
//            System.out.println("TELEFONO:" + dto.getTel());
//            System.out.println("TITULAR:" + dto.getTitu());
//            System.out.println("DIRECCION:" + dto.getDireccion().getId() + "-" + dto.getDireccion().getDesc());            
//            System.out.println("CIUDAD:" + dto.getCiudad().getId() + "-" + dto.getCiudad().getDesc());
//
//            System.out.println("------------------------------------------------");
//        }
//        
//        
//            GomeriaDTO dto= gomeriaDAO.recuperarRegistro(gomeriaDTO);                   //con id
//            System.out.println("Id " + dto.getId());
//            System.out.println("DESCRIPCION:" + dto.getDesc());
//            System.out.println("HORA DE APERTURA:" + dto.getApe());
//            System.out.println("HORA DE CIERRE:" + dto.getCie());
//            System.out.println("TELEFONO:" + dto.getTel());
//            System.out.println("TITULAR:" + dto.getTitu());
//            System.out.println("DIRECCION:" + dto.getDireccion().getId() + "-" + dto.getDireccion().getDesc());            
//            System.out.println("CIUDAD:" + dto.getCiudad().getId() + "-" + dto.getCiudad().getDesc());
//            System.out.println("------------------------------------------------");
        
        





//USUARIO----------------------------------------------------------------------------

//
//        usuarioDTO = new UsuarioDTO();     //si
//        usuarioDTO.setId(2);                      //si
//        usuarioDTO.setDesc("Juan Gonzalez");
//        usuarioDTO.setLog("admin");
//        usuarioDTO.setPass("123456789");
//        usuarioDTO.setEst(false);
//
//
//       PerfilDTO perfil = new PerfilDTO();
//        perfil.setId(1);
//        usuarioDTO.setPerfil(perfil);
//              
//        usuarioDTO.setPerfil(new PerfilDTO(1));       //id
        
//        usuarioDAO = new UsuarioDAOIMP();                     //si
//        
//        usuarioDAO.eliminar(usuarioDTO);                                   //modificar agregar eliminar
//        
//        
        
//        for (UsuarioDTO p : usuarioDAO.recuperarRegistros()) {              //sin id
//            System.out.println("Id " + p.getId() );
//            System.out.println("DESCRIPCION:" + p.getDesc());
//            System.out.println("LOGIN:" + p.getLog());
//            System.out.println("PASS:" + p.getPass());
//            System.out.println("ESTADO:" + p.getEst());
//            System.out.println("PERFIL:" + p.getPerfil().getId() + "-" + p.getPerfil().getDesc());
//            System.out.println("------------------------------------------------");
//        }
        
//        
//            UsuarioDTO p= usuarioDAO.recuperarRegistro(usuarioDTO);                   //con id
//            System.out.println("Id " + p.getId());
//            System.out.println("DESCRIPCION:" + p.getDesc());
//            System.out.println("LOGIN:" + p.getLog());
//            System.out.println("PASS:" + p.getPass());
//            System.out.println("ESTADO:" + p.getEst());
//            System.out.println("PERFIL:" + p.getPerfil().getId() + "-" + p.getPerfil().getDesc());
//            System.out.println("------------------------------------------------");
        



//MENU ITEM SISTEMA---------------------------------------------------------------------------



//        menuitemSistemaDTO = new MenuItemSistemaDTO();     //si
//        menuitemSistemaDTO.setId(2);                      //si
//        menuitemSistemaDTO.setDesc("iiiiiiiiiiiiiiiiii");
//        menuitemSistemaDTO.setCom("comentario");
//
//
//        MenuSistemaDTO menuSistema = new MenuSistemaDTO();
//        menuSistema.setId(1);
//        menuitemSistemaDTO.setMenuSistema(menuSistema);
//              
//        menuitemSistemaDTO.setMenuSistema(new MenuSistemaDTO(2));       //id
//        
//        menuitemSistemaDAO = new MenuItemSistemaDAOIMP();                 //si
        
//        menuitemSistemaDAO.eliminar(menuitemSistemaDTO);                              //modificar agregar eliminar
        
        
        
//        for (MenuItemSistemaDTO dto : menuitemSistemaDAO.recuperarRegistros()) {              //sin id
//            System.out.println("Id " +dto.getId() );
//            System.out.println("DESCRIPCION:" + dto.getDesc());
//            System.out.println("COMENTARIO:" + dto.getCom());
//            System.out.println("MENU SISTEMA:" + dto.getMenuSistema().getId() + "-" + dto.getMenuSistema().getDesc());
//            System.out.println("------------------------------------------------");
//        }
//        
//        
//            MenuItemSistemaDTO dto= menuitemSistemaDAO.recuperarRegistro(menuitemSistemaDTO);                   //con id
//            System.out.println("Id " +dto.getId() );
//            System.out.println("DESCRIPCION:" + dto.getDesc());
//            System.out.println("COMENTARIO:" + dto.getCom());
//            System.out.println("MENU SISTEMA:" + dto.getMenuSistema().getId() + "-" + dto.getMenuSistema().getDesc());
//            System.out.println("------------------------------------------------");

//PERSONAS--------------------------------------------------------------------------



        personaDTO = new PersonaDTO();     //si
        personaDTO.setId(2);                      //si
//        personaDTO.setNroCedula(1404250);
//        personaDTO.setNombres("Perla de los Angeles");
//        personaDTO.setApellidos("Aveiro Rodriguez");
//        personaDTO.setDireccion("EEEEEEEEEEEEEEEEEE");
//        Date fecha=Date.valueOf("1996-11-08");
//        personaDTO.setFechaNac(fecha);
//
//        CiudadDTO ciudad = new CiudadDTO();
//        ciudad.setId(1);
//        personaDTO.setCiudad(ciudad);
//        
//        NacionalidadDTO nacionalidad = new NacionalidadDTO();
//        nacionalidad.setId(1);
//        personaDTO.setNacionalidad(nacionalidad);
//        
//        personaDTO.setCiudad(new CiudadDTO(4));                     //ID
//        personaDTO.setNacionalidad(new NacionalidadDTO(1));               //ID
//
        personaDAO = new PersonaDAOIMP();                               //si
//         
//        personaDAO.eliminar(personaDTO);                                     //modificar agregar eliminar
        
        
        
//        for (PersonaDTO dto : personaDAO.recuperarRegistros()) {                        //sin id
//            System.out.println("Id " + dto.getId() );
//            System.out.println("Nro. Cedula " + dto.getNroCedula());
//            System.out.println("Nombres " + dto.getNombres());
//            System.out.println("Apellidos " + dto.getApellidos());
//            System.out.println("Fecha Nac " + dto.getFechaNac());
//            System.out.println("Dirección " + dto.getDireccion());
//            System.out.println("Ciudad " + dto.getCiudad().getId()+ "-" + dto.getCiudad().getDesc());
//            System.out.println("Nacionalidad " + dto.getNacionalidad().getId()+ "-" + dto.getNacionalidad().getDesc());
//            System.out.println("------------------------------------------------");
//        }
        
        
            PersonaDTO dto= personaDAO.recuperarRegistro(personaDTO);                   //con id
            System.out.println("Id " + dto.getId() );
            System.out.println("Nro. Cedula " + dto.getNroCedula());
            System.out.println("Nombres " + dto.getNombres());
            System.out.println("Apellidos " + dto.getApellidos());
            System.out.println("Fecha Nac " + dto.getFechaNac());
            System.out.println("Dirección " + dto.getDireccion());
            System.out.println("Ciudad " + dto.getCiudad().getId()+ "-" + dto.getCiudad().getDesc());
            System.out.println("Nacionalidad " + dto.getNacionalidad().getId()+ "-" + dto.getNacionalidad().getDesc());
            System.out.println("------------------------------------------------");
        



        
        
        
    }
    
    

    
    
    public static void main(String[] args) {
      new Test();
        
        
    }
    }
    